/**
* Test de plusieurs méthodes de tris sur des tableaux
* @author SEVETTE Matiss
*/
import java.util.Arrays;

class TrisTableau {
    long cpt;
    SimplesTableau monSpleTab = new SimplesTableau();

    /**
    * point d’entrée de l’exécution
    */
    void principal() {
    // appel des cas de test un par un

    testRechercheSeq();
    testVerifTri();
    testCreerTabAleatoire();
    testRechercheDicho();
    testTriSimple();
    testSeparer();
    testTriRapide();
    testCreerTabFreq();
    testTriParComptageFreq();
    testTriABulles();

    testRechercheSeqEfficacite();
    testRechercheDichoEfficacite();
    testTriSimpleEfficacite();
    testTriRapideEfficacite();
    testTriParComptageFreqEfficacite();
    testTriABullesEfficacite();
    }
    
    /**
    * Recherche séquentielle d'une valeur dans un tableau. La valeur à rechercher peut exister en plusieurs exemplaires mais la recherche
    * s'arrête dès qu'une première valeur est trouvée. On suppose que le tableau passé en paramètre est créé et possède au moins une
    * valeur (nbElem > 0). Ceci ne doit donc pas être vérifié.
    * @param leTab - le tableau dans lequel effectuer la recherche
    * @param nbElem - le nombre d'entiers que contient le tableau
    * @param aRech - l'entier à rechercher dans le tableau
    * @return l'indice (>=0) de la position de l'entier dans le tableau ou -1 s'il n'est pas présent
    */
    int rechercheSeq(int[] leTab, int nbElem, int aRech) {

        int ret = -1;
        int i = 0;
        boolean trouve = false;
      
        while (i < nbElem && !trouve) {
          if (leTab[i] == aRech) {
            trouve = true;
            ret = i;
          }
          cpt++;
          i++;
        }
        return ret;
      }
    
    /**
    * test de la méthode rechercheSeq
    */
    void testRechercheSeq() {
        System.out.println();
        System.out.println("testRechercheSeq : ");

        //Cas Normaux
        int[] tab1 = {2,4,6,0};
        testCasRerchercheSeq(tab1, 3, 4, 1);
        int[] tab2 = creerTabAleatoire(150, 100, 100);
        testCasRerchercheSeq(tab2, 150, 100, 0);
        //Cas limites
        int[] tab6 = {233};
        testCasRerchercheSeq(tab6, 1, 4, -1);
        int[] tab7 = {233};
        testCasRerchercheSeq(tab7, 1, 233, 0);
    }

	/**
	* teste un appel de verifTab
    * @param leTab le tableau dont on doit vérifier l'existence (leTab différent de null)
    * @param nbElem le nombre d'éléments que contiendra le tableau, doit vérifier la condition 0 < nbElem <= leTab.length
	* @param repAtt resultat attendu
	**/ 
    void testCasRerchercheSeq (int[] leTab, int nbElem, int aRech, int repAtt) {
        int tmp;
        tmp = rechercheSeq(leTab, nbElem, aRech);

        if (tmp == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /*
     * Test de l'efficacite de la methode rechercheSeq
     */
    void testRechercheSeqEfficacite() { // θ(n)
        System.out.println("testRechercheSeqEfficacite : ");

        int n = 100000;
        int resultat;
        double tmp1, tmp2, tmpFinal;

        for(int i = 0; i < 4; i++) {
            int[] tab = new int[n];
            cpt = 0;
            tmp1 = System.nanoTime();
            rechercheSeq(tab, n, 38);
            tmp2 = System.nanoTime();
            tmpFinal = tmp2 - tmp1;
            tmpFinal = tmpFinal / 1000000;
            resultat = (int) (cpt/n);
            System.out.println((i+1) + " n = " + n + " : " + tmpFinal + " ms, " 
              + resultat + " cpt/n");
            n = n * 2; 
        }
    }

    /**
    * Rempli un tableau d'une longueur donnée avec des nombres aléatoires
    * @param taille - la taille que devra faire le tableau
    * @param nbMin - le nombre aléatoire minimum
    * @param nbMax - le nombre aléatoire maximum
    * @return le tableau de taille taille 
    */
    int[] creerTabAleatoire(int taille, int nbMin, int nbMax) {
        int[] tab = {-1};
        if (taille <= 0 || nbMin > nbMax) {
            System.out.println("ERREUR : Tableau incorrect");
        } else {
            tab = new int[taille];
            int i = 0;
            while (i < tab.length) {
                int nbAleatoire = nbMin + (int) (Math.random() * ((nbMax - nbMin) + 1));
                tab[i] = nbAleatoire;
                i++;
            }
        }
        return tab;
    }

    /**
    * test de la méthode creerTabAleatoire
    */
    void testCreerTabAleatoire() {
        System.out.println();
        System.out.println("testCreerTabAleatoire : ");

        //Cas Normaux
        int[] tab1 = creerTabAleatoire(-5, 5, 10);
        System.out.println(Arrays.toString(tab1));
        int[] tab2 = creerTabAleatoire(0, 5, 10);
        System.out.println(Arrays.toString(tab2));
        int[] tab3 = creerTabAleatoire(10, 10, 5);
        System.out.println(Arrays.toString(tab3));
        int[] tab4 = creerTabAleatoire(10, 5, 10);
        System.out.println(Arrays.toString(tab4));
        int[] tab5 = creerTabAleatoire(6, 2, 100);
        System.out.println(Arrays.toString(tab5));

        //Cas limites
        int[] tab6 = creerTabAleatoire(1, 1, 1);
        System.out.println(Arrays.toString(tab6));
        int[] tab7 = creerTabAleatoire(10, 0, 0);
        System.out.println(Arrays.toString(tab7));
    }

    /**
    * Vérifie que le tableau passé en paramètre est trié par ordre croissant des valeurs. On suppose que le tableau passé en paramètre est
    * créé et possède au moins une valeur (nbElem > 0). Ceci ne doit donc pas être vérifié.
    * @param leTab - le tableau à vérifier (trié en ordre croissant)
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @return true si le tableau est trié
    */
    
    boolean verifTri(int[] leTab, int nbElem){
        boolean rep = true;
            for(int i = 0; i < nbElem - 1; i++) {
                if (leTab[i] > leTab[i+1]) {
                    rep = false;
                }
            }
        return rep;
    }

    /**
    * test de la méthode verifTri
    */
    void testVerifTri () {
        System.out.println();
        System.out.println("testVerifTri : ");

        //Cas Normaux
        int[] tab1 = {2,4,6,0};
        testCasVerifTri(tab1, 4, false);
        int[] tab2 = {2,4,6,8};
        testCasVerifTri(tab2, 4, true);
        int[] tab3 = {-2,4,6,6};
        testCasVerifTri(tab3, 4, true);

        //Cas limites
        int[] tab4 = {2};
        testCasVerifTri(tab4, 1, true);
        int[] tab5 = {2,4};
        testCasVerifTri(tab5, 2, true);
    }

    /**
	* teste un appel de verifTri
    * @param leTab - le tableau à vérifier (trié en ordre croissant)
    * @param nbElem - le nombre d'entiers présents dans le tableau
	* @param repAtt resultat attendu
	**/ 
    void testCasVerifTri (int[] leTab, int nbElem, boolean repAtt) {
        boolean tmp;
        tmp = verifTri(leTab, nbElem);

        if (tmp == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Recherche dichotomique d'une valeur dans un tableau. On suppose que le tableau est trié par ordre croissant. La valeur à
    * rechercher peut exister en plusieurs exemplaires, dans ce cas, c'est la valeur à l'indice le + faible qui sera trouvé. On suppose
    * également que le tableau passé en paramètre est créé et possède au moins une valeur (nbElem > 0). Ceci ne doit donc pas être
    * vérifié.
    * @param leTab - le tableau trié par ordre croissant dans lequel effectuer la recherche)
    * @param nbElem - le nombre d'entiers que contient le tableau
    * @param aRech - l'entier à rechercher dans le tableau
    * @return l'indice (>=0) de la position de l'entier dans le tableau ou -1 s'il n'est pas présent
    */
    int rechercheDicho(int[] leTab, int nbElem, int aRech) {
        // variables locales
        // indD = indice de début du sous-tableau
        // indF = indice de fin du sous-tableau
        // indM = indice milieu entre indD et indF
        int indD, indF, indM, ret;

        // initialisations
        indD = 0;
        indF = nbElem - 1;

        // boucle
        while ( indD != indF ) {
            indM = ( indD + indF ) / 2; // division entière !
            if ( aRech > leTab[indM] ) {
                indD = indM + 1;
            } else {
                indF = indM;
            }
            cpt++;
        }
        // terminaison : indD == indF forcément
        // MAIS leTab [indD] par forcément = aRech !!
        if ( aRech == leTab [indD] ) ret = indD;
        else ret = -1;
        return ret;
    }

    /*
    * Test de l'efficacite de la methode rechercheDicho
    */
    void testRechercheDichoEfficacite () { // θ(log2n)
        System.out.println("testRechercheDichoEfficacite : ");

        int n = 100000;
        double resultat;
        double tmp1, tmp2, tmpFinal;

        for(int i = 0; i < 4; i++) {
            int[] tab = new int[n];
            cpt = 0;
            tmp1 = System.nanoTime();
            rechercheDicho(tab, n, 38);
            tmp2 = System.nanoTime();
            tmpFinal = tmp2 - tmp1;
            tmpFinal = tmpFinal / 1000000;
            resultat = (cpt/(Math.log(n)/Math.log(2)));
            System.out.println((i+1) + " n = " + n + " : " + tmpFinal + " ms, " 
              + resultat + " cpt/log2n");
            n = n * 2; 
        }
    } 

    /**
    * test de la méthode rechercheDicho
    */
    void testRechercheDicho () {
        System.out.println();
        System.out.println("testRechercheDicho : ");

        //Cas Normaux
        int[] tab1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        testCasRechercheDicho(tab1, 10, 5, 4);
        testCasRechercheDicho(tab1, 10, 1, 0);
        testCasRechercheDicho(tab1, 10, 10, 9);
        testCasRechercheDicho(tab1, 10, 11, -1);
        testCasRechercheDicho(tab1, 10, 0, -1);
        int[] tab3 = {1,1,1,1,1};
        testCasRechercheDicho(tab3, 5, 1, 0);

        //Cas limites
        int[] tab2 = {1};
        testCasRechercheDicho(tab2, 1, 1, 0);
        testCasRechercheDicho(tab2, 1, 2, -1);
        testCasRechercheDicho(tab2, 1, 0, -1);
    }

    /**
	* teste un appel de rechercheDicho
    * @param leTab - le tableau trié par ordre croissant dans lequel effectuer la recherche)
    * @param nbElem - le nombre d'entiers que contient le tableau
    * @param aRech - l'entier à rechercher dans le tableau
	* @param repAtt resultat attendu
	**/ 
    void testCasRechercheDicho(int[] leTab, int nbElem, int aRech, int repAtt) {
        int rep = rechercheDicho(leTab, nbElem, aRech);
        if (rep == repAtt) {
            System.out.println("Test reussi");
        } else {
            System.out.println("Echec du test");
        }
    }

    /**
    * Tri par ordre croissant d'un tableau selon la méthode simple : l'élément minimum est placé en début de tableau (efficacité en n
    * carré). On suppose que le tableau passé en paramètre est créé et possède au moins une valeur (nbElem > 0). Ceci ne doit donc pas
    * être vérifié.
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
    */
    void triSimple(int[] leTab, int nbElem) {
        int i = 0;
        int j = 0;
        int min = leTab[0];
        int indiceMin = 0;

        while (i < nbElem - 1) {
            j = i;
            indiceMin = i;
            min = leTab[i];
            while(j < nbElem){
                if(min > leTab[j]){
                    min = leTab[j];
                    indiceMin = j;
                }
                cpt++;
                j++; 
            }
            monSpleTab.echange(leTab, nbElem, i, indiceMin);
            i++;
        }
        cpt++;
    }

    /*
    * Test de l'efficacite de la methode triSimple
    */
    void testTriSimpleEfficacite () { // θ(n2)
        System.out.println("testTriSimpleEfficacite : ");

        int n = 1000;
        double resultat;
        double tmp1, tmp2, tmpFinal;

        for(int i = 0; i < 4; i++) {
            //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui même un int
            int[] tab = new int[n];

            //creation d'un tableau de taille n qui contient en ordre décroissant des valeurs de 1 à n
            for(int j = 0; j < n; j++){
                tab[j] = n - j;
            }

            cpt = 0;
            tmp1 = System.nanoTime();
            triSimple(tab, n);
            tmp2 = System.nanoTime();
            tmpFinal = tmp2 - tmp1;
            tmpFinal = tmpFinal / 1000000;
            //calcul du resultat en faisant cpt divisé par n au carré
            resultat = cpt/(Math.pow(n, 2));
            System.out.println((i+1) + " n = " + n + " : " + tmpFinal + " ms, " 
              + resultat + " cpt/n2");
            n = n * 2; 
        }
    }

    /**
    * test de la méthode triSimple
    */
    void testTriSimple () {
        System.out.println();
        System.out.println("testTriSimple : ");

        //Cas Normaux
        int[] tab1 = {1, 2, 3, 4, 5};
        int[] tab2 = {1, 2, 3, 4, 5};
        testCasTriSimple(tab1, 5, tab2);
        int[] tab3 = {5, 4, 3, 2, 1};
        int[] tab4 = {1, 2, 3, 4, 5};
        testCasTriSimple(tab3, 5, tab4);
        int[] tab5 = {1, 1, 1, 1, 1};
        int[] tab6 = {1, 1, 1, 1, 1};
        testCasTriSimple(tab5, 5, tab6);
        int[] tab7 = {1,9,8,7,6,5,1,3,4,5};
        int[] tab8 = {1,1,3,4,5,5,6,7,8,9};
        testCasTriSimple(tab7, 10, tab8);
        int[] tab9 = {-5, -6,-4, 6,-4, 5, 4, 3, 2, 1};
        int[] tab10 = {-6,-5,-4,-4,1,2,3,4,5,6};
        testCasTriSimple(tab9, 10, tab10);

        //Cas limites
        int[] tab11 = {1};
        int[] tab12 = {1};
        testCasTriSimple(tab11, 1, tab12);
        int[] tab13 = {94,2};
        int[] tab14 = {2,94};
        testCasTriSimple(tab13, 2, tab14);
    }


	/**
	* teste un appel de triSimple
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
	* @param repAtt resultat attendu
	**/ 
    void testCasTriSimple (int[] leTab, int nbElem, int[] repAtt) {
        triSimple(leTab, nbElem);
        boolean egalite;
        egalite = Arrays.equals(leTab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Cette méthode renvoie l’indice de séparation du tableau en 2 parties par placement du pivot à la bonne case.
    * @param tab - le tableau des valeurs
    * @param indR - indice Right de fin de tableau
    * @param indL - indice Left de début de tableau
    * @return l’indice de séparation du tableau
    */
    int separer(int[] tab, int indL, int indR) {
        boolean droiteGauche = true;
        int nbElem = indR+1;

        while (indL != indR) {
            if (droiteGauche) {
                if (tab[indR] < tab[indL]) {
                    monSpleTab.echange(tab, nbElem, indL, indR);
                    droiteGauche = false;
                } else {
                    indR--;
                }
            } else {
                if (tab[indL] > tab[indR]) {
                    monSpleTab.echange(tab, nbElem, indL, indR);
                    droiteGauche = true;
                } else {
                    indL++;
                }
            }
            cpt++;
        }
        return indL;
    }

    /**
    * test de la méthode separer
    */
    void testSeparer() {
        System.out.println();
        System.out.println("testSeparer : ");

        //Cas Normaux
        int[] tab1 = {1, 2, 3, 4, 5};
        testCasSeparer(tab1, 0, 4, 0);
        int[] tab2 = {5, 4, 3, 2, 1};
        testCasSeparer(tab2, 0, 4, 4);
        int[] tab3 = {1, 1, 1, 1, 1};
        testCasSeparer(tab3, 0, 4, 0);
        int[] tab4 = {1,9,8,7,6,5,1,3,4,5};
        testCasSeparer(tab4, 0, 9, 0);
        int[] tab5 = {-5, -6,-4, 6,-4, 5, 4, 3, 2, 1};
        testCasSeparer(tab5, 0, 9, 1);
        //Cas limites
        int[] tab6 = {1};
        testCasSeparer(tab6, 0, 0, 0);
        int[] tab7 = {94,2};
        testCasSeparer(tab7, 0, 1, 1);    
    }

    /**
	* teste un appel de separer
    * @param tab - le tableau des valeurs
    * @param indR - indice Right de fin de tableau
    * @param indL - indice Left de début de tableau
	**/ 
    void testCasSeparer (int[] tab, int indL, int indR, int repAtt) {
        int indiceSeparation = separer(tab, indL, indR);
        if (indiceSeparation == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Méthode de tri récursive selon le principe de séparation. La méthode s'appelle elle-même sur les tableaux gauche et droite par
    * rapport à un pivot.
    * @param tab - le tableau sur lequel est effectué la séparation
    * @param indL - l'indice gauche de début de tableau
    * @param indR - l'indice droite de fin de tableau
    */
    void triRapideRec(int[] tab, int indL, int indR) {
        // variable locale
        int indS; // indS = indice de séparation
        indS = separer (tab, indL, indR );

        if ( (indS-1) > indL ) {
            triRapideRec ( tab, indL, (indS-1) );
        }

        if ( (indS+1) < indR ) {
            triRapideRec ( tab, (indS+1), indR );
        }
    }

    /**
    * Tri par ordre croissant d'un tableau selon la méthode du tri rapide (QuickSort). On suppose que le tableau passé en paramètre est
    * créé et possède au moins une valeur (nbElem > 0). Ceci ne doit donc pas être vérifié. Cette méthode appelle triRapideRec(...) qui elle
    * effectue réellement le tri rapide selon la méthode de séparation récursive.
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
    */
    void triRapide(int[] leTab, int nbElem) {
        triRapideRec (leTab, 0, (nbElem-1));
    }

    /*
    * Test de l'efficacite de la methode triRapide
    */
    void testTriRapideEfficacite () { // θ(nlog2n)
        System.out.println("testTriRapideEfficacite : ");

        int n = 100000;
        double resultat;
        double tmp1, tmp2, tmpFinal;

        for(int i = 0; i < 4; i++) {
            //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui même un int
            int[] tab = new int[n];

            //On met que des valeurs aléatoires dans le tableau tab
            for(int j = 0; j < n; j++) {
                tab[j] = (int)(Math.random() * n);
            }

            //System.out.println(Arrays.toString(tab));
            cpt = 0;
            tmp1 = System.nanoTime();
            triRapide(tab, n);
            tmp2 = System.nanoTime();
            tmpFinal = tmp2 - tmp1;
            tmpFinal = tmpFinal / 1000000;
            //calcul du resultat en faisant cpt divisé par nlog2n
            resultat = cpt/(n*(Math.log(n)/Math.log(2)));
            System.out.println((i+1) + " n = " + n + " : " + tmpFinal + " ms, " 
              + resultat + " cpt/nlog2n");
            n = n * 2; 
        }
    }

    /**
    * test de la méthode triRapide
    */
    void testTriRapide() {
        System.out.println();
        System.out.println("testTriRapide : ");

        //Cas Normaux
        int[] tab1 = {1, 2, 3, 4, 5};
        int[] repAtt1 = {1, 2, 3, 4, 5};
        testCasTriRapide(tab1, 5, repAtt1);
        int[] tab2 = {10, 9, 8, 6};
        int[] repAtt2 = {6, 8, 9, 10};
        testCasTriRapide(tab2, 4, repAtt2);
        int[] tab3 = {1, 1, 1, 1, 1};
        int[] repAtt3 = {1, 1, 1, 1, 1};
        testCasTriRapide(tab3, 5, repAtt3);
        int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
        int[] repAtt4 = {1, 3, 4, 5, 5, 6, 7, 7, 8, 9};
        testCasTriRapide(tab4, 10, repAtt4);
        int[] tab5 = {-5, -6,-4, 6,-4, 5, 4, 3, 2, 1};
        int[] repAtt5 = {-6, -5, -4, -4, 1, 2, 3, 4, 5, 6};
        testCasTriRapide(tab5, 10, repAtt5);

        //Cas limites
        int[] tab6 = {1};
        int[] repAtt6 = {1};
        testCasTriRapide(tab6, 1, repAtt6);
        int[] tab7 = {94,2};
        int[] repAtt7 = {2, 94};
        testCasTriRapide(tab7, 2, repAtt7);
    }

    /**
	* teste un appel de triRapide
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
    * @param repAtt resultat attendu
	**/ 
    void testCasTriRapide (int[] leTab, int nbElem, int[] repAtt) {
        triRapide(leTab, nbElem);
        boolean egalite;
        egalite = Arrays.equals(leTab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * A partir d'un tableau initial passé en paramètre "leTab", cette méthode renvoie un nouveau tableau "tabFreq" d'entiers où chaque
    * case contient la fréquence d'apparition des valeurs dans le tableau initial. Pour simplifier, on suppose que le tableau initial ne contient
    * que des entiers compris entre 0 et max (>0). Dès lors le tableau "tabFreq" se compose de (max+1) cases et chaque case "i"
    * (0<=i<=max) contient le nombre de fois que l'entier "i" apparait dans le tableau initial. On suppose que le tableau passé en
    * paramètre est créé et possède au moins une valeur (nbElem > 0). Ceci ne doit donc pas être vérifié. Par contre, on vérifiera que le
    * max du tableau initial est >= min et que le min est >= 0. Dans le cas contraire, renvoyer un tableau "null".
    * @param leTab - le tableau initial
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @return le tableau des fréquences de taille (max+1) ou null si la méthode ne s'applique pas
    */
    int[] creerTabFreq(int[] leTab, int nbElem) {
        int max = 0;
        int i = 0;
        boolean neg = false;
        int[] tabFreq;

        while(i < nbElem && !neg) {
            if(leTab[i] < 0) {
                neg = true;
            } else {
                if(leTab[i] > max) {
                    max = leTab[i];
                }
            }
            i++;
            cpt++;
        }
        if(neg==false) {
            tabFreq = new int[max+1];
            for(int j = 0; j < nbElem; j++) {
                tabFreq[leTab[j]]++;
            }
        } else {
            System.err.println("Erreur creerTabFreq : le tab invalide");
            tabFreq = null;
        }
        return tabFreq;
    }

    /**
    * test de la méthode creerTabFreq
    */
    void testCreerTabFreq() {
        System.out.println();
        System.out.println("testCreerTabFreq : ");

        //Cas Normaux
        int[] tab1 = {1, 2, 3, 4, 5};
        int[] repAtt1 = {0, 1, 1, 1, 1, 1};
        testCasCreerTabFreq(tab1, 5, repAtt1);
        int[] tab2 = {10, 9, 8, 6};
        int[] repAtt2 = {0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1};
        testCasCreerTabFreq(tab2, 4, repAtt2);
        int[] tab3 = {1, 1, 1, 1, 1};
        int[] repAtt3 = {0, 5};
        testCasCreerTabFreq(tab3, 5, repAtt3);
        int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
        int[] repAtt4 = {0, 1, 0, 1, 1, 2, 1, 2, 1, 1};
        testCasCreerTabFreq(tab4, 10, repAtt4);

        //Cas limites
        int[] tab7 = {1};
        int[] repAtt7 = {0, 1};
        testCasCreerTabFreq(tab7, 1, repAtt7);
        int[] tab8 = {1,2};
        int[] repAtt8 = {0, 1, 1};
        testCasCreerTabFreq(tab8, 2, repAtt8);

        //Cas d'erreur
        int[] tab5 = {-5,-6,-4, 6,-4, 5, 4, 3, 2, 1};
        int[] tab6 = null;
        testCasCreerTabFreq(tab5, 10, tab6);
    }

    /**
	* teste un appel de creerTabFreq
    * @param leTab - le tableau initial
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param repAtt resultat attendu
	**/ 
    void testCasCreerTabFreq (int[] leTab, int nbElem, int[] repAtt) {
        int[] tabFreq = creerTabFreq(leTab, nbElem);
        boolean egalite;
        egalite = Arrays.equals(tabFreq, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }


    /**
    * Tri par ordre croissant d'un tableau selon la méthode du tri par comptage de fréquences. On suppose que le tableau passé en
    * paramètre est créé et possède au moins une valeur (nbElem > 0). Ceci ne doit donc pas être vérifié.
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
    */
    void triParComptageFreq(int[] leTab, int nbElem){
        int[] tabFreq = creerTabFreq(leTab, nbElem);
        
        if(tabFreq != null) {
            int i = 0;
            int repetition = 0;
            int p = 0;
            while(i < tabFreq.length) {
                if(tabFreq[i] != 0) {
                    while(repetition<tabFreq[i]) {
                        leTab[p] = i;
                        p++;
                        repetition++;
                        cpt++;
                    }
                    repetition = 0;
                }
                i++;
            }
        } else {
            leTab = null;
            System.err.println("Erreur triParComptageFreq : le tab invalide");
            System.out.println(Arrays.toString(leTab));
        }
    }

    /*
    * Test de l'efficacite de la methode triParComptageFreq
    */
    void testTriParComptageFreqEfficacite () { // θ(nlog2n)
        System.out.println("testTriParComptageFreqEfficacite : ");

        int n = 100000;
        double resultat;
        double tmp1, tmp2, tmpFinal;

        for(int i = 0; i < 4; i++) {
            //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui même un int
            int[] tab = new int[n];

            //On met que des valeurs aléatoires mais inférieur à n dans le tableau tab
            for(int j = 0; j < n; j++) {
                tab[j] = (int)(Math.random()*n);
            }
            // On récupère la valeur la plus grande du tableau tab
            int max = monSpleTab.leMax(tab, tab.length);
            
            //System.out.println(Arrays.toString(tab));
            cpt = 0;
            tmp1 = System.nanoTime();
            triParComptageFreq(tab, n); 
            tmp2 = System.nanoTime();
            tmpFinal = tmp2 - tmp1;
            tmpFinal = tmpFinal / 1000000;
            resultat = (double) cpt/(n+max);
            System.out.println((i+1) + " n = " + n + " : " + tmpFinal + " ms, " 
              + resultat + " cpt/(n+max)");
            n = n * 2; 
        }
    }

    /**
    * test de la méthode triParComptageFreq
    */
    void testTriParComptageFreq() {
        System.out.println();
        System.out.println("testTriParComptageFreq : ");

        //Cas Normaux
        int[] tab1 = {1, 2, 3, 4, 5};
        int[] repAtt1 = {1, 2, 3, 4, 5};
        testCasTriParComptageFreq(tab1, 5, repAtt1);
        int[] tab2 = {10, 9, 8, 6};
        int[] repAtt2 = {6, 8, 9, 10};
        testCasTriParComptageFreq(tab2, 4, repAtt2);
        int[] tab3 = {1, 1, 1, 1, 1};
        int[] repAtt3 = {1, 1, 1, 1, 1};
        testCasTriParComptageFreq(tab3, 5, repAtt3);
        int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
        int[] repAtt4 = {1, 3, 4, 5, 5, 6, 7, 7, 8, 9};
        testCasTriParComptageFreq(tab4, 10, repAtt4);

        //Cas limites
        int[] tab7 = {1};
        int[] repAtt7 = {1};
        testCasTriParComptageFreq(tab7, 1, repAtt7);
        int[] tab8 = {1,2};
        int[] repAtt8 = {1, 2};
        testCasTriParComptageFreq(tab8, 2, repAtt8);
    }

    /**
	* teste un appel de triParComptageFreq
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
    * @param repAtt resultat attendu
	**/ 
    void testCasTriParComptageFreq (int[] leTab, int nbElem, int[] repAtt) {
        triParComptageFreq(leTab, nbElem);
        boolean egalite;
        egalite = Arrays.equals(leTab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Tri par ordre croissant d'un tableau selon la méthode du tri à bulles : tant que le tableau n'est pas trié, permuter le contenu de 2
    * cases successives si leTab[i] > leTab[i+1]. On suppose que le tableau passé en paramètre est créé et possède au moins une valeur
    * (nbElem > 0). Ceci ne doit donc pas être vérifié.
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
    */
    void triABulles(int[] leTab, int nbElem){
        boolean tri = false;
        int i = 0;
        int temp = 0;
        while(!tri) {
            tri = true;
            i = 0;
            while(i < nbElem-1) {
                if(leTab[i] > leTab[i+1]) {
                    temp = leTab[i];
                    leTab[i] = leTab[i+1];
                    leTab[i+1] = temp;
                    tri = false;
                    cpt++;
                }
                i++;
            }
        }
    }

    /*
    * Test de l'efficacite de la methode triABulles
    */
    void testTriABullesEfficacite () { // θ(n2)
        System.out.println("testTriABullesEfficacite : ");

        int n = 1000;
        double resultat;
        double tmp1, tmp2, tmpFinal;

        for(int i = 0; i < 4; i++) {
            //creation d'un tableau tab qui contient que des int, le tableau fait la taille de n qui est lui même un int
            int[] tab = new int[n];

            //creation d'un tableau de taille n qui contient en ordre décroissant des valeurs de 1 à n
            for(int j = 0; j < n; j++){
                tab[j] = n - j;
            }

            cpt = 0;
            tmp1 = System.nanoTime();
            triABulles(tab, n);
            tmp2 = System.nanoTime();
            tmpFinal = tmp2 - tmp1;
            tmpFinal = tmpFinal / 1000000;
            //calcul du resultat en faisant cpt divisé par n au carré
            resultat = cpt/(Math.pow(n, 2));
            System.out.println((i+1) + " n = " + n + " : " + tmpFinal + " ms, " 
              + resultat + " cpt/n2");
            n = n * 2; 
        }
    }

    /**
    * test de la méthode triABulles
    */
    void testTriABulles() {
        System.out.println();
        System.out.println("testTriABulles : ");

        //Cas Normaux
        int[] tab1 = {1, 2, 3, 4, 5};
        int[] repAtt1 = {1, 2, 3, 4, 5};
        testCasTriABulles(tab1, 5, repAtt1);
        int[] tab2 = {10, 9, 8, 6};
        int[] repAtt2 = {6, 8, 9, 10};
        testCasTriABulles(tab2, 4, repAtt2);
        int[] tab3 = {1, 1, 1, 1, 1};
        int[] repAtt3 = {1, 1, 1, 1, 1};
        testCasTriABulles(tab3, 5, repAtt3);
        int[] tab4 = {7,9,8,7,6,5,1,3,4,5};
        int[] repAtt4 = {1, 3, 4, 5, 5, 6, 7, 7, 8, 9};
        testCasTriABulles(tab4, 10, repAtt4);

        //Cas limites
        int[] tab7 = {1};
        int[] repAtt7 = {1};
        testCasTriABulles(tab7, 1, repAtt7);
        int[] tab8 = {1,2};
        int[] repAtt8 = {1, 2};
        testCasTriABulles(tab8, 2, repAtt8);
    }



    
    /**
	* teste un appel de triABulles
    * @param leTab - le tableau à trier par ordre croissant
    * @param nbElem - le nombre d'entiers que contient le tableau
	**/ 
    void testCasTriABulles(int[] leTab, int nbElem, int[] repAtt) {
        triABulles(leTab, nbElem);
        boolean egalite;
        egalite = Arrays.equals(leTab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }
}