/**
* Test de plusieurs méthodes sur des tableaux
* @author SEVETTE Matiss
*/

import java.util.Arrays;

class SimplesTableau {

    /**
    * point d’entrée de l’exécution
    */
    void principal() {
    // appel des cas de test un par un
    testVerifTab();
    testAfficherTab();
    testTirerAleatoire();
    testRemplirAleatoire();
    testEgalite();
    testCopier();
    testLeMax();
    testInverse();
    testEchange();
    testMelange();
    testDecalerGche();
    testSupprimerUneValeur();;
    testInclusion();
    testRemplirToutesDiff();
    }
    
    /**
    * Factorisation : appelé chaque fois qu'une vérification doit se faire sur la validité d'un tableau. Le tableau n'est pas valide si celui-ci est inexistant 
    * ou si le nombre d'éléments qu'il contiendra est incompatible avec sa taille (leTab.length)
    * @param leTab le tableau dont on doit vérifier l'existence (leTab différent de null)
    * @param nbElem le nombre d'éléments que contiendra le tableau, doit vérifier la condition 0 < nbElem <= leTab.length
    * @return vrai ssi le tableau est valide
    */
    boolean verifTab(int[] leTab, int nbElem ) {
        boolean ret = true;

        if(leTab == null){
            ret = false;
            System.out.println("Tableau inexistant");
        } else {
            if (nbElem <= 0 || nbElem > leTab.length) {
                ret = false;
                System.out.println("nbElem hors des limites");
            }
        }
        return ret;
    }

    /**
    * test de la méthode verifTab
    */
    void testVerifTab() {
        System.out.println();
        System.out.println("testVerifTab : ");

        //Cas Normaux
        int[] tab1 = null;
        testCasVerifTab(tab1,5,false);
        int[] tab2 = {23, 100};
        testCasVerifTab(tab2,-1,false);
        int[] tab3 = {23,100};
        testCasVerifTab(tab3,10,false);
        int[] tab4 = {2,4,6,0};
        testCasVerifTab(tab4,3,true);

        //Cas limites
        int[] tab5 = {};
        testCasVerifTab(tab5,0,false);
        int[] tab6 = {233};
        testCasVerifTab(tab6,1,true);
    }

	/**
	* teste un appel de verifTab
    * @param tab le tableau dont on doit vérifier l'existence (leTab différent de null)
    * @param nb le nombre d'éléments que contiendra le tableau, doit vérifier la condition 0 < nbElem <= leTab.length
	* @param repAtt resultat attendu
	**/ 
    void testCasVerifTab (int[] tab, int nb, boolean repAtt) {
        boolean tmp;
        tmp = verifTab(tab, nb);

        if (tmp == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Affiche le contenu des nbElem cases d'un tableau une par une. Tenir compte des cas d'erreurs concernant leTab et nbElem (appel de verifTab).
    * @param leTab le tableau à afficher
    * @param nbElem le nombre d'entiers que contient le tableau
    */
    void afficherTab(int[] leTab, int nbElem) {
        boolean correct;
        correct = verifTab(leTab, nbElem);

        if (correct) {
            int i = 0;
            while (i < nbElem) {
                System.out.print(leTab[i] + "\t");
                i++;
            }
            System.out.println();
        } else { //je ne travaille pas
            System.err.println("Erreur afficherTab : leTab invalide");
        }
    }

    /**
    * test de la méthode afficherTab
    */
    void testAfficherTab () {
        System.out.println();
        System.out.println("testAfficherTab : ");

        //Cas Normaux
        int[] tab1 = null;
        afficherTab(tab1,5); System.out.println();
        int[] tab2 = {23, 100};
        afficherTab(tab2,-1); System.out.println();
        int[] tab3 = {23,100};
        afficherTab(tab3,10); System.out.println();
        int[] tab4 = {2,4,6,0};
        afficherTab(tab4,3);System.out.println();

        //Cas limites
        int[] tab5 = {};
        afficherTab(tab5,0);System.out.println();
        int[] tab6 = {233};
        afficherTab(tab6,1);System.out.println();
    }

    /**
    * Renvoie un entier aléatoire compris entre min et max (min <= valeur <= max). Vérifier que min <= max et min >= 0, sinon afficher une erreur (et dans ce cas retourner -1).
    * @param min - la valeur de l'entier minimum
    * @param max - la valeur de l'entier maximum
    * @return l'entier aléatoire
    */
    int tirerAleatoire(int min, int max) {
        int ret;

        if(min < 0 || min > max) {
            System.out.println("min hors limite");
            ret = -1;
        } else {
            ret = min + (int)(Math.random() * (max - min + 1));
        }

        return ret;
    }

    /**
    * test de la méthode tirerAleatoire
    */
    void testTirerAleatoire() {
        System.out.println();
        System.out.println();
        System.out.println("testTirerAleatoire : ");
        
        //Cas Normaux
        int min1 = -1; int max1 = 5;
        testCasTirerAleatoire(min1, max1); System.out.println();
        int min2 = 4; int max2 = 2;
        testCasTirerAleatoire(min2, max2); System.out.println();
        int min3 = 2; int max3 = 5;
        testCasTirerAleatoire(min3, max3); System.out.println();

        //Cas Limites
        int min4 = 10; int max4 = 10;
        testCasTirerAleatoire(min4, max4); System.out.println();

        int min5 = 0; int max5 = 0;
        testCasTirerAleatoire(min5, max5); System.out.println();
    }

    /**
	* teste un appel de tirerAleatoire
    * @param min - la valeur de l'entier minimum
    * @param max - la valeur de l'entier maximum
	**/ 
    void testCasTirerAleatoire (int min, int max) {
        int i = 0;
        int tmp = tirerAleatoire(min, max);

        if (tmp != -1) {
            for(i = 0; i < 9; i++) {
                tmp = tirerAleatoire(min, max);
                System.out.println(tmp);
            }
        }
    }

    /**
    * A partir d'un tableau créé, remplir aléatoirement ce tableau de nbElem valeurs comprises entre min et max. 
    * Tenir compte des cas d'erreurs concernant leTab et nbElem (appel de verifTab). 
    * Vérifier que min <= max, sinon afficher également une erreur. Utiliser obligatoirement la méthode "int tirerAleatoire (int min, int max)".
    * @param leTab - le tableau à remplir de valeurs tirées aléatoirement
    * @param nbElem - le nombre d'entiers que contiendra le tableau
    * @param min - la valeur de l'entier minimum
    * @param max - la valeur de l'entier maximum
    */
    void remplirAleatoire(int[] leTab, int nbElem, int min, int max) {
        boolean correct;
        correct = verifTab(leTab, nbElem);

        int tmp = tirerAleatoire(min, max);

        if (correct && tmp != -1) {
            int i = 0;
            while (i < nbElem) {
                leTab[i] = tirerAleatoire(min, max);
                i++;
            }

            i = 0;
            while (i < leTab.length) {
                System.out.print(leTab[i] + "\t");
                i++;
            }
            System.out.println();
        } else { //je ne travaille pas
            System.err.println("Erreur remplirAleatoire : leTab ou nbElem " 
              + "ou min invalide");
        }

    }

    /**
    * test de la méthode remplirleatoire
    */
    void testRemplirAleatoire() {
        System.out.println();
        System.out.println();
        System.out.println("testRemplirAleatoire : ");
        
        //Cas Normaux
        int[] tab1 = null;
        remplirAleatoire(tab1, 0, 5, 2); System.out.println();
        int[] tab2 = {23, 100};
        remplirAleatoire(tab2, 3, 0, 2); System.out.println();
        int[] tab3 = {23,100};
        remplirAleatoire(tab3, -1, -1, 2); System.out.println();
        int[] tab4 = {2,4,6,0} ;
        remplirAleatoire(tab4, 3, 15, 60);System.out.println();

        //Cas limites
        int[] tab5 = {};
        remplirAleatoire(tab5, 0, 2, 2); System.out.println();
        int[] tab6 = {233};
        remplirAleatoire(tab6, 1, 50, 50);System.out.println();
    }

    /**
    * Renvoie vrai si les 2 tableaux passés en paramètre sont exactement les mêmes en nombre d'éléments et en contenu (case par case). 
    * Tenir compte des cas d'erreurs concernant tab1, nbElem1 et tab2, nbElem2 (appel de verifTab 2 fois) et afficher un éventuel message d'erreur (et dans ce cas renvoyer faux).
    * @param tab1 - le 1er tableau à comparer
    * @param tab2 - le 2ème tableau à comparer
    * @param nbElem1 - le nombre d'entiers présents dans le 1er tableau
    * @param nbElem2 - le nombre d'entiers présents dans le 2ème tableau
    * @return true si égalité parfaite sinon false
    */
    boolean egalite(int[] tab1, int[] tab2, int nbElem1, int nbElem2) {
        boolean ret = true;
        boolean correct1;
        correct1 = verifTab(tab1, nbElem1);

        boolean correct2;
        correct2 = verifTab(tab2, nbElem2);

        if (correct1 && correct2) {
            if (nbElem1 == nbElem2) {
                int i = 0;
                while (i < nbElem1 && ret) {
                    if (tab1[i] != tab2[i]){
                        ret = false;
                    }
                    i++;
                }
            } else {
                System.out.println("nbElem différents");
                ret = false;
            }
        } else { //je ne travaille pas
            System.err.println("Erreur egalite : les tab sont invalides");
            ret = false;
        }
        return ret;
    }

    /**
    * test de la méthode egalite
    */
    void testEgalite() {
        System.out.println();
        System.out.println();
        System.out.println("testEgalite : ");
        
        //Cas Normaux
        int[] tab1 = null; int[] tab2 = {1,2,3};
        testCasEgalite(tab1, tab2, 0, 3, false); 
        System.out.println();
        int[] tab3 = {23, 100}; int[] tab4 = {1};
        testCasEgalite(tab3, tab4, -1, 3, false); 
        System.out.println();
        int[] tab5 = {23, 100}; int[] tab6 = {23,100,2};
        testCasEgalite(tab5, tab6, 2, 3, false); 
        System.out.println();
        int[] tab7 = {1,2,3,4}; int[] tab8 = {1,2,3,4};
        testCasEgalite(tab7, tab8, 3, 3, true); 
        System.out.println();

        //Cas limites
        int[] tab9 = {}; int[] tab10 = {};
        testCasEgalite(tab9, tab10, 0, 0, false); 
        System.out.println();
        int[] tab11 = {1,2,3,4}; int[] tab12 = {1,2,3,4};
        testCasEgalite(tab11, tab12, 4, 4, true); 
        System.out.println();
        int[] tab13 = {1}; int[] tab14 = {1};
        testCasEgalite(tab13, tab14, 1, 1, true); 
        System.out.println();
    }

	/**
	* teste un appel de egalite
    * @param tab1 - le 1er tableau à comparer
    * @param tab2 - le 2ème tableau à comparer
    * @param nbElem1 - le nombre d'entiers présents dans le 1er tableau
    * @param nbElem2 - le nombre d'entiers présents dans le 2ème tableau
    * @param repAtt resultat attendu
	**/ 
    void testCasEgalite (int[] tab1, int[] tab2, int nbElem1, int nbElem2, 
      boolean repAtt) {
        boolean tmp;
        tmp = egalite(tab1, tab2, nbElem1, nbElem2);

        if (tmp == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Renvoie la copie exacte (clone) du tableau passé en paramètre.
    * @param tabToCopy - le tableau à copier
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @return le nouveau tableau qui est la copie du tableau passé en paramètre
    */
    int[] copier(int[] tabToCopy, int nbElem) {
        int[] tab = new int[nbElem];
        if (tabToCopy == null) {
            tab = null;
        } else {
            for(int i = 0; (i < nbElem); i++) {
                tab[i] = tabToCopy[i];
            }
        }
        return tab; 
    }

    /**
    * test de la méthode copier
    */
    void testCopier() {
        System.out.println();
        System.out.println();
        System.out.println("testCopier : ");
        
        //Cas Normaux
        int[] tab1 = null; int[] tab2 = null;
        testCasCopier(tab1, 0, tab2); System.out.println();
        int[] tab3 = {1,2,3}; int[] tab4 = {};
        testCasCopier(tab3, 0, tab4); System.out.println();
        int[] tab5 = {1,2,3}; int[] tab6 = {1,2};
        testCasCopier(tab5, 2, tab6); System.out.println();

        //Cas limites
        int[] tab7 = {1,2,3}; int[] tab8 = {1,2,3};
        testCasCopier(tab7, 3, tab8); System.out.println();
        int[] tab9 = {}; int[] tab10 = {};
        testCasCopier(tab9, 0, tab10); System.out.println();
        int[] tab11 = {5}; int[] tab12 = {5};
        testCasCopier(tab11, 1, tab12); System.out.println();
    }

	/**
	* teste un appel de copier
    * @param tabToCopy - le tableau à copier
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param repAtt resultat attendu
	**/ 
    void testCasCopier (int[] tabToCopy, int nbElem, int[] repAtt) {
        int[] tab;
        tab = copier(tabToCopy, nbElem);
        boolean egalite;
        egalite = Arrays.equals(tab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Renvoie le maximum parmi les éléments du tableau.
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @return le maximum des éléments du tableau
    */
    int leMax(int[] leTab, int nbElem) {
        int ret = leTab[0];

        int i = 0;
        while (i < nbElem) {
            if (leTab[i] > ret) {
                ret = leTab[i];
            }
            i++;
        }
        return ret; 
    }

    /**
    * test de la méthode leMax
    */
    void testLeMax() {
        System.out.println();
        System.out.println();
        System.out.println("testLeMax : ");
        
        //Cas Normaux
        int[] tab2 = {1,2,3}; 
        testCasLeMax(tab2, 2, 2); System.out.println();
        int[] tab3 = {1,5,2,5,3}; 
        testCasLeMax(tab3, 5, 5); System.out.println();


        //Cas limites
        int[] tab4 = {1,1,1,1,1}; 
        testCasLeMax(tab4, 5, 1); System.out.println();
        int[] tab5 = {2,2,2}; 
        testCasLeMax(tab5, 1, 2); System.out.println();
        int[] tab6 = {10}; 
        testCasLeMax(tab6, 1, 10); System.out.println();
    }

	/**
	* teste un appel de leMax
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param repAtt resultat attendu
	**/ 
    void testCasLeMax (int[] leTab, int nbElem, int repAtt) {
        int max;
        max = leMax(leTab, nbElem);

        if (max == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Renvoie un nouveau tableau qui est l'inverse de celui passé en paramètre. Son jème élément est égal au (nbElem+1-j) élément du tableau initial 
    * (dans l'explication, j=1 signifie premier élément du tableau).
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @return le nouveau tableau qui est l'inverse de leTab sur la plage (0...nbElem-1)
    */
    int[] inverse(int[] leTab, int nbElem) {
        int[] res = new int [leTab.length];

        for(int i = 0; i < nbElem; i++) { //On inverse jusqu'à nbElem
            int index = nbElem - 1 - i;
            res[i] = leTab[index];
        }

        for (int i = nbElem; i < leTab.length; i++) { //On remet les valeurs depuis nbElem
            res[i] = leTab[i];
        }
        return res;
    }

    /**
    * test de la méthode inverse
    */
    void testInverse() {
        System.out.println();
        System.out.println();
        System.out.println("testInverse : ");
        
        //Cas Normaux
        int[] tab3 = {1,2,3}; int[] tab4 = {2,1,3};
        testCasInverse(tab3, 2, tab4); System.out.println();

        //Cas limites
        int[] tab7 = {}; int[] tab8 = {};
        testCasInverse(tab7, 0, tab8); System.out.println();
        int[] tab9 = {1}; int[] tab10 = {1};
        testCasInverse(tab9, 1, tab10); System.out.println();
        int[] tab11 = {1,2,3,4,5}; int[] tab12 = {5,4,3,2,1};
        testCasInverse(tab11, 5, tab12); System.out.println();
    }

	/**
	* teste un appel de inverse
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param repAtt resultat attendu
	**/ 
    void testCasInverse (int[] leTab, int nbElem, int[] repAtt) {
        int[] tab;
        tab = inverse(leTab, nbElem);
        boolean egalite;
        egalite = Arrays.equals(tab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }


    /**
    * Echange les contenus des cases du tableau passé en paramètre, cases identifiées par les indices ind1 et ind2. Vérifier que les indices ind1 et ind2 sont 
    * bien compris entre zéro et (nbElem-1), sinon afficher un message d'erreur.
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param ind1 - numéro de la première case à échanger
    * @param ind2 - numéro de la deuxième case à échanger
    */
    void echange(int[] leTab, int nbElem, int ind1, int ind2) {

        if (0 <= ind1 && ind1 <= (nbElem - 1) && 0 <= ind2 && 
          ind2 <= (nbElem - 1)) {
            int tmp = leTab[ind1];
            leTab[ind1] = leTab[ind2];
            leTab[ind2] = tmp;
        } else { 
            System.out.println("ind1 ou ind2 hors limite");
        }
    }

    /**
    * test de la méthode echange
    */
    void testEchange() {
        System.out.println();
        System.out.println();
        System.out.println("testEchange : ");
        
        //Cas Normaux
        int[] tab1 = {1,2,3}; int[] tab2 = {2,1,3};
        testCasEchange(tab1, 2, 0, 1, tab2); 
        System.out.println();
        int[] tab3 = {1,2,3}; int[] tab4 = {1,2,3};
        testCasEchange(tab3, 1, 0, 1, tab4); 
        System.out.println();
        int[] tab5 = {1,2,3}; int[] tab6 = {1,2,3};
        testCasEchange(tab5, 2, -1, 5, tab6); 
        System.out.println();
        int[] tab11 = {1,2,3,4}; int[] tab12 = {1,3,2,4};
        testCasEchange(tab11, 4, 1, 2, tab12); 
        System.out.println();

        //Cas limites
        int[] tab7 = {1}; int[] tab8 = {1};
        testCasEchange(tab7, 1, 0, 0, tab8); 
        System.out.println();
        int[] tab9 = {1,2,3,4}; int[] tab10 = {4,2,3,1};
        testCasEchange(tab9, 4, 0, 3, tab10); 
        System.out.println();

    }

	/**
	* teste un appel de echange
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param ind1 - numéro de la première case à échanger
    * @param ind2 - numéro de la deuxième case à échanger
    * @param repAtt resultat attendu
	**/ 
    void testCasEchange (int[] leTab, int nbElem, int ind1, int ind2, 
      int[] repAtt) {
        echange(leTab, nbElem, ind1, ind2);
        boolean egalite;
        egalite = Arrays.equals(leTab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Retourne un nouveau tableau qui a la même taille et les mêmes occurrences d'élements que le tableau passé en paramètre mais ces
    * éléments sont répartis selon des indices aléatoires (0 <= indice <= nbElem-1). Une technique simple consiste à utiliser les méthodes
    * "echange" et "tirerAleatoire" pour effectuer le mélange.
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @return le nouveau tableau qui a le même contenu que le tableau initial mais mélangé
    */
    int[] melange(int[] leTab, int nbElem) {
        int[] res = copier(leTab, leTab.length);

        for(int i = 0; (i < nbElem - 1); i++) {
            int tmp1 = tirerAleatoire(i, (nbElem - 1));
            int tmp2 = tirerAleatoire(i, (nbElem - 1));
            echange(res, nbElem, tmp1, tmp2);
        }
        return res;
    }

    /**
    * test de la méthode melange
    * Etant donné que la méthode nous renvoie un tableau rempli aléatoirement, il est difficilement possible de comparer
    * le tableau de base et celui qui est retourné pour vérifier que le résultat est correct. Une solution serait de lister
    * tous les cas possibles mais cette méthode est trop périlleuse, car il peut y avoir un nombre de possibilité très grand.
    * Il se peut que même avec un mélange, le tableau reste inchangé car, par pur hasard, les valeurs sont retournées à leur indice de base.
    * Un contrôle visuel est donc nécessaire. Il faut vérifier que les valeurs ont changé d'indice (si on n'a pas la malchance de retomber sur
    * un même tableau qui est resté inchangé) et que toutes les valeurs présentes au début se retrouvent après le mélange.
    */
    void testMelange() {
        System.out.println();
        System.out.println();
        System.out.println("testMelange : ");
        
        //Cas Normaux
        int[] tab1 = {10,20,30,40,50,60,70}; 
        int[] tab2 = melange(tab1, 5);
        System.out.println(Arrays.toString(tab2)); System.out.println();
        int[] tab3 = {10,20,30}; 
        int[] tab4 = melange(tab3, 2);
        System.out.println(Arrays.toString(tab4)); System.out.println();
 
        //Cas limites
        int[] tab5 = {10}; 
        int[] tab6 = melange(tab5, 1);
        System.out.println(Arrays.toString(tab6)); System.out.println();
        int[] tab8 = {10,20,30,40}; 
        int[] tab9 = melange(tab8, 4);
        System.out.println(Arrays.toString(tab9)); System.out.println();
    }

    /**
    * Décale de une case de la droite vers la gauche toutes les cases d'un tableau à partir d'un indice "ind" et 
    * jusque nbElem-1 ([ind]<-[ind+1]<-[ind+2]<-...<-[nbElem-2]<-[nbElem-1]). Vérifier que ind est compris entre 0 et (nbElem-2) sinon afficher une erreur.
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param ind - l'indice à partir duquel commence le décalage à gauche
    */
    void decalerGche(int[] leTab, int nbElem, int ind) {

        if(ind >= 0 && ind <= (nbElem-2)) {
            for(int i = ind; i < (nbElem - 1); i++) {
                leTab[i] = leTab[i+1];
            }
        } else { //je ne travaille pas
            System.err.println("ind invalide");
        }
    }

    /**
    * test de la méthode decalerGche
    */
    void testDecalerGche() {
        System.out.println();
        System.out.println();
        System.out.println("testDecalerGche : ");
        
        //Cas Normaux
        int[] tab1 = {1,2,3,4,5}; int[] tab2 = {2,3,4,4,5};
        testCasDecalerGche(tab1, 4, 0, tab2);
        System.out.println();
        int[] tab3 = {1,2,3,4,5}; int[] tab4 = {2,2,3,4,5};
        testCasDecalerGche(tab3, 2, 0, tab4);
        System.out.println();
        int[] tab5 = {1,2,3,4,5}; int[] tab6 = {1,2,3,4,5};
        testCasDecalerGche(tab5, 2, 1, tab6);
        System.out.println();
        int[] tab7 = {1,2,3,4,5}; int[] tab8 = {1,2,3,4,5};
        testCasDecalerGche(tab7, 5, -1, tab8);
        System.out.println();

        //Cas limites
        int[] tab9 = {1,2,3,4,5}; int[] tab10 = {2,3,4,5,5};
        testCasDecalerGche(tab9, 5, 0, tab10);
        System.out.println();
        int[] tab11 = {1,2,3,4,5}; int[] tab12 = {1,2,3,4,5};
        testCasDecalerGche(tab11, 0, 0, tab12);
        System.out.println();
        int[] tab13 = {1,2,3,4,5}; int[] tab14 = {1,2,3,5,5};
        testCasDecalerGche(tab13, 5, 3, tab14);
        System.out.println();
    }

	/**
	* teste un appel de decalerGche
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param ind - l'indice à partir duquel commence le décalage à gauche
    * @param repAtt resultat attendu
	**/ 
    void testCasDecalerGche (int[] leTab, int nbElem, int ind, int[] repAtt) {
        decalerGche(leTab, nbElem, ind);
        boolean egalite;
        egalite = Arrays.equals(leTab, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }


    /**
    * Supprime du tableau la première case rencontrée dont le contenu est égale à "valeur". La case du tableau est supprimée par
    * décalage à gauche des cases du tableau. L'appel de la méthode "decalerGche" est obligatoire. A l'issue de la suppression (si elle
    * existe) le nombre d'éléments dans le tableau est décrémenté et retourné.
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param valeur - le contenu de la première case à supprimer
    * @return le nombre d'éléments dans le tableau (éventuellement inchangé)
    */
    int supprimerUneValeur(int[] leTab, int nbElem, int valeur) {
        int ret = leTab.length;
        
        boolean present = false;
        int i = 0;
        while (i < nbElem && present == false) {
            if (leTab[i] == valeur) {
                present = true;
                int ind = i;
                if(ind <= (nbElem-2)) {
                    decalerGche(leTab, nbElem, ind);
                    int[] tab = copier(leTab, nbElem-1);
                    ret = tab.length;
                    System.out.println(Arrays.toString(tab));
                } else {
                    System.out.println("Impossible de supprimer car " +
                      " l'emplacement de la valeur est supérieure à nbElem-2," +
                        " donc decalerGche ne travaille pas");
                }
            }
            i++;
        }
        return ret;
    }

    /**
    * test de la méthode supprimeUneValeur
    */
    void testSupprimerUneValeur() {
        System.out.println();
        System.out.println();
        System.out.println("testSupprimerUneValeur : ");
        
        //Cas Normaux
        int[] tab1 = {1,2,3,4,5}; 
        testCasSupprimerUneValeur(tab1, 3, 2, 2);
        System.out.println();
        int[] tab2 = {1,2,3,4,5}; 
        testCasSupprimerUneValeur(tab2, 4, 3, 3);
        System.out.println();
        int[] tab3 = {1,2,3,4,5}; 
        testCasSupprimerUneValeur(tab3, 4, 4, 5);
        System.out.println();

        //Cas limites
        int[] tab4 = {1,2}; 
        testCasSupprimerUneValeur(tab4, 2, 1, 1);
        System.out.println();
        int[] tab5 = {1,2,3,4,5}; 
        testCasSupprimerUneValeur(tab5, 5, 2, 4);
        System.out.println();
    }

	/**
	* teste un appel de supprimerUneValeur
    * @param leTab - le tableau
    * @param nbElem - le nombre d'entiers présents dans le tableau
    * @param valeur - le contenu de la première case à supprimer
    * @param repAtt resultat attendu
	**/ 
    void testCasSupprimerUneValeur (int[] leTab, int nbElem, int valeur, 
      int repAtt) {
        int nb;
        nb = supprimerUneValeur(leTab, nbElem, valeur);
        if (nb == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Renvoie vrai ssi le tableau tab1 est inclus dans tab2. Autrement dit, si tous les éléments de tab1 se retrouvent intégralement dans
    * tab2 (y compris les doublons) mais pas nécessairement dans le même ordre. L'utilisation de méthodes déjà écrites est autorisé
    * @param tab1 - le premier tableau
    * @param tab2 - le deuxième tableau
    * @param nbElem1 - le nombre d'entiers présents dans le tableau1
    * @param nbElem2 - le nombre d'entiers présents dans le tableau2
    * @return vrai ssi tableau1 est inclus dans tableau2
    */
    boolean inclusion(int[] tab1, int[] tab2, int nbElem1, int nbElem2) {
        boolean rep1 = true;

        if(nbElem1 < 0 || nbElem1 > tab1.length || nbElem2 < 0 
          || nbElem2 > tab2.length) {
            System.out.println("nbElem1 ou nbElem2 hors limite");
            rep1 = false;
        } else {
            int i = 0;
            int j = 0;
            while (i < nbElem1 && rep1 == true) {
                rep1 = false;
                while (j < nbElem2 && rep1 == false) {
                    if (tab1[i] == tab2[j]) {
                        rep1 = true;
                    }
                    j++;
                }
                j = 0;
                i++;
            }
        }
        return rep1;
    }

    /**
    * test de la méthode inclusion
    */
    void testInclusion() {
        System.out.println();
        System.out.println();
        System.out.println("testInclusion : ");
        
        //Cas Normaux
        int[] tab1 = {1,2,3}; int[] tab2 = {1,2,3,4,5};
        testCasInclusion(tab1, tab2, 3, 5, true); 
        System.out.println();
        int[] tab3 = {1,2,3}; int[] tab4 = {1,2,3,4,5};
        testCasInclusion(tab3, tab4, 3, 2, false); 
        System.out.println();
        int[] tab5 = {1,2,1,2}; int[] tab6 = {1,2,3,4,5};
        testCasInclusion(tab5, tab6, 4, 2, true); 
        System.out.println();
        int[] tab7 = {1,2,1,2}; int[] tab8 = {1,2,3,4,5};
        testCasInclusion(tab7, tab8, 4, 3, true); 
        System.out.println();
        int[] tab11 = {1,2,1,2}; int[] tab12 = {1,2,3,4,5};
        testCasInclusion(tab11, tab12, 5, -1, false); 
        System.out.println();

        //Cas limites
        int[] tab9 = {1}; int[] tab10 = {1};
        testCasInclusion(tab9, tab10, 1, 1, true); 
        System.out.println();
    }

	/**
	* teste un appel de inclusion
    * @param tab1 - le premier tableau
    * @param tab2 - le deuxième tableau
    * @param nbElem1 - le nombre d'entiers présents dans le tableau1
    * @param nbElem2 - le nombre d'entiers présents dans le tableau2
    * @param repAtt resultat attendu
	**/ 
    void testCasInclusion (int[] tab1, int[] tab2, int nbElem1, int nbElem2, 
      boolean repAtt) {
        boolean tmp;
        tmp = inclusion(tab1, tab2, nbElem1, nbElem2);

        if (tmp == repAtt) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * A partir d'un tableau déjà créé, remplir le tableau de nbElem valeurs saisies par l'utilisateur. Au fur et à mesure de la saisie, si la
    * nouvelle valeur saisie existe déjà dans le tableau alors ne pas l'insérer et demander de ressaisir. Tenir compte des cas d'erreurs
    * concernant leTab et nbElem (appel de verifTab).
    * @param leTab - le tableau à remplir d'éléments tous différents
    * @param nbElem - le nombre d'entiers que contiendra le tableau
    */
    void remplirToutesDiff(int[] leTab, int nbElem) {
        boolean correct;
        correct = verifTab(leTab, nbElem);

        if (correct) {
            System.out.println(Arrays.toString(leTab));
            int i = 0;
            int j = 0;
            while (i < (nbElem)) {
                int rep = SimpleInput.getInt("Choisir un nombre pour remplacer "
                  + leTab[i] + " : ");
                while (j < leTab.length) {
                    if (rep == leTab[j]) {
                        rep = SimpleInput.getInt("Choisir un autre nombre : ");
                        j = -1;
                    }
                    j++;
                }
                leTab[i] = rep;
                j = 0;
                i++;
                System.out.println(Arrays.toString(leTab));
            }
        } else { //je ne travaille pas
            System.err.println("Erreur remplirToutesDiff : leTab invalide");
        }
    }

    /**
    * test de la méthode remplirToutesDiff
    */
    void testRemplirToutesDiff() {
        System.out.println();
        System.out.println();
        System.out.println("testRemplirToutesDiff : ");
        
        //Cas Normaux
        int[] tab1 = null;
        remplirToutesDiff(tab1, 0); System.out.println();
        int[] tab2 = {23, 100};
        remplirToutesDiff(tab2, 3); System.out.println();
        int[] tab3 = {23,100};
        remplirToutesDiff(tab3, -1); System.out.println();
        int[] tab4 = {2,4,6,0} ;
        remplirToutesDiff(tab4, 3);System.out.println();

        //Cas limites
        int[] tab5 = {};
        remplirToutesDiff(tab5, 0); System.out.println();
        int[] tab6 = {233};
        remplirToutesDiff(tab6, 1);System.out.println();
        int[] tab7 = {1,2,3,4};
        remplirToutesDiff(tab7, 4);System.out.println();
    }
}
