/**
* TP R1.01.P2 - Semaine 50 et 51
* @author SEVETTE Matiss
*/
import java.util.Arrays;
import java.util.ArrayList;

class Planification1 {
    Duree duree = new Duree(0);

    /**
     * point d’entrée de l’exécution
     */
    void principal() {
        testAffichages();
        testAfficherHorairesEtDureeTrajets2Gares();
    }

    void testAffichages() {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        remplirLesCollections(trajets, horaires);
        afficherHorairesEtDureeTousTrajets(trajets, horaires);
        //afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Vannes", "Redon");
    }

    /**
    * Remplit les 2 collections trajets et horaires avec les données suivantes (mais vous pouvez bien entendu en ajouter autant que vous voulez)
    * @param trajets - la collection des trajets
    * @param horaires - la collection des horaires
    */
    void remplirLesCollections(ArrayList<String> trajets, ArrayList<Integer> horaires) {
        trajets.add("1");
        trajets.add("TER");
        trajets.add("Vannes");
        trajets.add("Redon");
        horaires.add(1);
        horaires.add(9);
        horaires.add(35);
        horaires.add(10);
        horaires.add(30);

        trajets.add("2");
        trajets.add("TGV");
        trajets.add("Vannes");
        trajets.add("Redon");
        horaires.add(2);
        horaires.add(8);
        horaires.add(0);
        horaires.add(10);
        horaires.add(5);

        trajets.add("3");
        trajets.add("TER");
        trajets.add("Redon");
        trajets.add("Nantes");
        horaires.add(3);
        horaires.add(11);
        horaires.add(0);
        horaires.add(12);
        horaires.add(30);

        trajets.add("4");
        trajets.add("TER");
        trajets.add("Redon");
        trajets.add("Nantes");
        horaires.add(4);
        horaires.add(14);
        horaires.add(15);
        horaires.add(15);
        horaires.add(37);

        trajets.add("5");
        trajets.add("TGV");
        trajets.add("Vannes");
        trajets.add("Nantes");
        horaires.add(5);
        horaires.add(10);
        horaires.add(3);
        horaires.add(12);
        horaires.add(11);

        trajets.add("6");
        trajets.add("TGV");
        trajets.add("Vannes");
        trajets.add("Nantes");
        horaires.add(6);
        horaires.add(11);
        horaires.add(25);
        horaires.add(13);
        horaires.add(38);
    }

    /**
    * Il s’agit donc de parcourir correctement les 2 collections pour récupérer :
    * dans la collection trajets, la clé d’identification du trajet, le type de train et les gares de départ et d’arrivée,
    * dans la collection horaires, les heures et minutes (heure dep, min dep, heure arr, min arr) du départ et de l’arrivée correspondant à la clé d’identification du trajet.
    * @param trajets - la collection des trajets
    * @param horaires - la collection des horaires
    */
    void afficherHorairesEtDureeTousTrajets(ArrayList<String> trajets, ArrayList<Integer> horaires) {

        for (int i = 0; i < trajets.size(); i = i + 4) {

            for (int j = 0; j < horaires.size(); j = j + 5) {

                int id = Integer.parseInt(trajets.get(i));
                if (id == horaires.get(j)) {
                    int arriveeH = horaires.get(j + 3);
                    int arriveeM = horaires.get(j + 4);
                    int departH = horaires.get(j + 1);
                    int departM = horaires.get(j + 2);

                    Duree depart = new Duree(departH, departM, 0);
                    Duree arrivee = new Duree(arriveeH, arriveeM, 0);
                    System.out.println("Train " + trajets.get(i + 1) + " numéro " + trajets.get(i) + " : ");
                    System.out.println("\t Départ de " + trajets.get(i + 2) + " à " + depart.enTexte('H'));
                    System.out.println("\t Arrivée à " + trajets.get(i + 3) + " " + arrivee.enTexte('H'));
                    // Afficher la duree du trajet en soustrayant l'heure d'arrivée à l'heure de
                    // départ (en utilisant la classe Duree)
                    arrivee.soustraire(depart);
                    System.out.println("\t Duree du trajet : " + arrivee.enTexte('H'));
                }
            }
        }
    }

    /**
    * C’est le même principe que la méthode afficherHorairesEtDureeTousTrajets mais on se limite cette
    * fois aux seules gares de départ et d’arrivée passées en paramètre (trajets directs).
    * @param trajets - la collection des trajets
    * @param horaires - la collection des horaires
    * @param gareDep - la gare de départ
    * @param gareDest - la gare d’arrivée
    */
    void afficherHorairesEtDureeTrajets2Gares(ArrayList<String> trajets, ArrayList<Integer> horaires, String gareDep, String gareDest) {

        for (int i = 0; i < trajets.size(); i = i + 4) {

            for (int j = 0; j < horaires.size(); j = j + 5) {

                int id = Integer.parseInt(trajets.get(i));
                if (id == horaires.get(j)) {
                    int arriveeH = horaires.get(j + 3);
                    int arriveeM = horaires.get(j + 4);
                    int departH = horaires.get(j + 1);
                    int departM = horaires.get(j + 2);

                    Duree depart = new Duree(departH, departM, 0);
                    Duree arrivee = new Duree(arriveeH, arriveeM, 0);
                    if (trajets.get(i + 2) == gareDep && trajets.get(i + 3) == gareDest) {
                        System.out.println("Train " + trajets.get(i + 1) + " numéro " + trajets.get(i) + " : ");
                        System.out.println("\t Départ de " + trajets.get(i + 2) + " à " + depart.enTexte('H'));
                        System.out.println("\t Arrivée à " + trajets.get(i + 3) + " " + arrivee.enTexte('H'));
                        // Afficher la duree du trajet en soustrayant l'heure d'arrivée à l'heure de
                        // départ (en utilisant la classe Duree)
                        arrivee.soustraire(depart);
                        System.out.println("\t Duree du trajet : " + arrivee.enTexte('H'));
                    }
                }
            }
        }
    }

    /**
    * test de la méthode afficherHorairesEtDureeTrajets2Gares
    */
    void testAfficherHorairesEtDureeTrajets2Gares() {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        remplirLesCollections(trajets, horaires);

        System.out.println();
        System.out.println("testAfficherHorairesEtDureeTrajets2Gares : ");
        System.out.println("TEST 1 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Vannes", "Redon");
        System.out.println("TEST 2 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Redon", "Nantes");
        System.out.println("TEST 3 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Vannes", "Nantes");
        System.out.println("TEST 4 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Nantes", "Redon");
        System.out.println("TEST 5 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Redon", "Vannes");
        System.out.println("TEST 6 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Nantes", "Vannes");
        System.out.println("TEST 7 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Redon", "Redon");
    }
}