/**
* TP R1.01.P2 - Semaine 50 et 51
* @author SEVETTE Matiss
*/
import java.util.*;

import javax.xml.transform.Source;

import java.io.*;

class Planification2 {
    Duree duree = new Duree(0);
    String nomDuFichierInfos = "TrajetsEtHoraires.txt"; //Le mettre dans le même dossier que le fichier java
    
    /**
     * point d’entrée de l’exécution
     */
    void principal() {
        
        testAfficherHorairesEtDureeTousTrajets();
        testAfficherHorairesEtDureeTrajets2Gares();
        
        testObtenirInfosUnTrajet();
        testObtenirInfosUnHoraire();
        testTrouverTousLesTrajets();
        testChercherCorrespondance();
      }

    void testAfficherHorairesEtDureeTousTrajets() {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        // Appel des méthodes codées dans la classe
        remplirLesCollections (trajets, horaires, nomDuFichierInfos);
        afficherHorairesEtDureeTousTrajets ( trajets, horaires );
    }

    /**
    * Remplacer le code de la méthode remplirLesCollections par une lecture de fichier texte dont le nom TrajetsEtHoraires.txt est passé en paramètre.
    * Une fois la ligne de texte lue (par exemple id trajet2 / type train / lieu dep / lieu arr), la récupération séparée de chaque information id trajet2, type
    * train, lieu dep, lieu arr se fera en utilisant la méthode String[] split(…) de la classe String.
    * @param trajets - la collection des trajets
    * @param horaires - la collection des horaires
    * @param nomFich - le nom du fichier texte à lire
    */
    void remplirLesCollections (ArrayList<String> trajets, ArrayList<Integer> horaires, String nomFich) {
        try{
            InputStream flux=new FileInputStream(nomFich); 
            InputStreamReader lecture=new InputStreamReader(flux);
            BufferedReader buff=new BufferedReader(lecture);
            String ligne;
            while ((ligne=buff.readLine())!=null){
                
                String[] split = ligne.split("/");

                int splitLength = split.length;

                for (int i = 0; i < split.length; i++) {

                    String sansBlanc = split[i].trim();

                    if (splitLength == 4) {
                        trajets.add(sansBlanc);
                    }

                    if (splitLength == 5) {
                        horaires.add(Integer.parseInt(sansBlanc));
                    }
                }
            }
            buff.close(); 
        }		
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    /**
    * Il s’agit donc de parcourir correctement les 2 collections pour récupérer :
    * dans la collection trajets, la clé d’identification du trajet, le type de train et les gares de départ et d’arrivée,
    * dans la collection horaires, les heures et minutes (heure dep, min dep, heure arr, min arr) du départ et de l’arrivée correspondant à la clé d’identification du trajet.
    * @param trajets - la collection des trajets
    * @param horaires - la collection des horaires
    */
    void afficherHorairesEtDureeTousTrajets(ArrayList<String> trajets, ArrayList<Integer> horaires) {

        for (int i = 0; i < trajets.size(); i = i + 4) {

            for (int j = 0; j < horaires.size(); j = j + 5) {

                int id = Integer.parseInt(trajets.get(i));
                if (id == horaires.get(j)) {
                    int arriveeH = horaires.get(j + 3);
                    int arriveeM = horaires.get(j + 4);
                    int departH = horaires.get(j + 1);
                    int departM = horaires.get(j + 2);

                    Duree depart = new Duree(departH, departM, 0);
                    Duree arrivee = new Duree(arriveeH, arriveeM, 0);
                    System.out.println("Train " + trajets.get(i + 1) + " numéro " + trajets.get(i) + " : ");
                    System.out.println("\t Départ de " + trajets.get(i + 2) + " à " + depart.enTexte('H'));
                    System.out.println("\t Arrivée à " + trajets.get(i + 3) + " " + arrivee.enTexte('H'));
                    // Afficher la duree du trajet en soustrayant l'heure d'arrivée à l'heure de
                    // départ (en utilisant la classe Duree)
                    arrivee.soustraire(depart);
                    System.out.println("\t Duree du trajet : " + arrivee.enTexte('H'));
                }
            }
        }
    }

    /**
    * C’est le même principe que la méthode afficherHorairesEtDureeTousTrajets mais on se limite cette
    * fois aux seules gares de départ et d’arrivée passées en paramètre (trajets directs).
    * @param trajets - la collection des trajets
    * @param horaires - la collection des horaires
    * @param gareDep - la gare de départ
    * @param gareDest - la gare d’arrivée
    */
    void afficherHorairesEtDureeTrajets2Gares(ArrayList<String> trajets, ArrayList<Integer> horaires, String gareDep, String gareDest) {

        for (int i = 0; i < trajets.size(); i = i + 4) {

            for (int j = 0; j < horaires.size(); j = j + 5) {

                int id = Integer.parseInt(trajets.get(i));
                if (id == horaires.get(j)) {
                    int arriveeH = horaires.get(j + 3);
                    int arriveeM = horaires.get(j + 4);
                    int departH = horaires.get(j + 1);
                    int departM = horaires.get(j + 2);

                    Duree depart = new Duree(departH, departM, 0);
                    Duree arrivee = new Duree(arriveeH, arriveeM, 0);

                    if (trajets.get(i + 2).equals(gareDep) && trajets.get(i + 3).equals(gareDest)) {
                        System.out.println("Train " + trajets.get(i + 1) + " numéro " + trajets.get(i) + " : ");
                        System.out.println("\t Départ de " + trajets.get(i + 2) + " à " + depart.enTexte('H'));
                        System.out.println("\t Arrivée à " + trajets.get(i + 3) + " " + arrivee.enTexte('H'));
                        // Afficher la duree du trajet en soustrayant l'heure d'arrivée à l'heure de
                        // départ (en utilisant la classe Duree)
                        arrivee.soustraire(depart);
                        System.out.println("\t Duree du trajet : " + arrivee.enTexte('H'));
                    }
                }
            }
        }
    }

    /**
    * test de la méthode afficherHorairesEtDureeTrajets2Gares
    */
    void testAfficherHorairesEtDureeTrajets2Gares() {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        remplirLesCollections (trajets, horaires, nomDuFichierInfos);

        System.out.println();
        System.out.println("testAfficherHorairesEtDureeTrajets2Gares : ");
        System.out.println("TEST 1 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Vannes", "Redon");
        System.out.println("TEST 2 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Redon", "Nantes");
        System.out.println("TEST 3 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Vannes", "Nantes");
        System.out.println("TEST 4 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Nantes", "Redon");
        System.out.println("TEST 5 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Redon", "Vannes");
        System.out.println("TEST 6 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Nantes", "Vannes");
        System.out.println("TEST 7 : ");
        afficherHorairesEtDureeTrajets2Gares(trajets, horaires, "Redon", "Redon");
    }


    /**
    * A partir d’un identifiant, la méthode renvoie dans un tableau de String 3 informations : le type de train, la gare de départ et la gare de destination.
    * @param idTrajet - l'identifiant du trajet que l'on rentre
    * @param trajets - la collection des trajets
    * @return le nouveau tableau qui renvoit les informations du trajet
    */
    String[] obtenirInfosUnTrajet (String idTrajet, ArrayList<String> trajets) {
        String[] infos = new String[3];
        
        for (int i = 0; i < trajets.size(); i = i + 4) {
            if (trajets.get(i).equals(idTrajet)) {
                infos[0] = trajets.get(i + 1);
                infos[1] = trajets.get(i + 2);
                infos[2] = trajets.get(i + 3);
            }
        }
        return infos;
    }

    /**
    * test de la méthode obtenirInfosUnTrajet
    */
    void testObtenirInfosUnTrajet () {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        remplirLesCollections (trajets, horaires, nomDuFichierInfos);
        
        System.out.println();
        System.out.println("testObtenirInfosUnTrajet : ");

        //Cas Normaux
        String[] str1 = {"TER", "Vannes", "Redon"};
        testCasObtenirInfosUnTrajet("1",trajets, str1);

        String[] str2 = {"TGV", "Vannes", "Nantes"};
        testCasObtenirInfosUnTrajet("5",trajets, str2);

        String[] str3 = {"TER", "Redon", "Nantes"};
        testCasObtenirInfosUnTrajet("4",trajets, str3);
        String[] str4 = {"TER", "Redon", "Nantes"};
        testCasObtenirInfosUnTrajet("3",trajets, str4);
    }

    /**
	* teste un appel de obtenirInfosUnTrajet
    * @param idTrajet - l'identifiant du trajet que l'on rentre
    * @param trajets - la collection des trajets
	* @param repAtt - resultat attendu
	**/ 
    void testCasObtenirInfosUnTrajet (String idTrajet, ArrayList<String> trajets, String[] repAtt) {
        String[] rep;
        rep = obtenirInfosUnTrajet(idTrajet, trajets);
        boolean egalite;
        egalite = Arrays.equals(rep, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * A partir d’un identifiant, la méthode renvoie dans un tableau d’entiers 4 informations : heure départ, minutes départ, heure arrivée, minutes arrivée.
    * @param idTrajet - l'identifiant du trajet que l'on rentre
    * @param horaires - la collection des horaires
    * @return le nouveau tableau qui renvoit les informations du trajet concernant les heures
    */
    int[] obtenirInfosUnHoraire (String idTrajet, ArrayList<Integer> horaires) {
        ArrayList<String> trajets = new ArrayList<String>();
        remplirLesCollections (trajets, horaires, nomDuFichierInfos);

        int[] heures = new int[4];
        for (int j = 0; j < horaires.size(); j = j + 5) {
            // Convertir la collection des horaires en String.
            // Si l'idTrajet est égal à l'integer qui définit l'idTrajet dans la collection horaires, 
            int id = Integer.parseInt(idTrajet);
            if (id == horaires.get(j)) {
                heures[0] = horaires.get(j + 1);
                heures[1] = horaires.get(j + 2);
                heures[2] = horaires.get(j + 3);
                heures[3] = horaires.get(j + 4);
            }
        }
        return heures;
    }

    /**
    * test de la méthode obtenirInfosUnHoraire
    */
    void testObtenirInfosUnHoraire () {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        remplirLesCollections (trajets, horaires, nomDuFichierInfos);
        
        System.out.println();
        System.out.println("testObtenirInfosUnHoraire : ");

        //Cas Normaux
        int[] int1 = {9, 35, 10, 30};
        testCasObtenirInfosUnHoraire("1", horaires, int1);

        int[] int2 = {10, 3, 12, 11};
        testCasObtenirInfosUnHoraire("5", horaires, int2);

        int[] int3 = {14, 15, 15, 37};
        testCasObtenirInfosUnHoraire("4", horaires, int3);
        int[] int4 = {11, 0, 12, 30};
        testCasObtenirInfosUnHoraire("3", horaires, int4);
    }

    /**
	* teste un appel de obtenirInfosUnHoraire
    * @param idTrajet - l'identifiant du trajet que l'on rentre
    * @param horaires - la collection des horaires
	* @param repAtt - resultat attendu
	**/ 
    void testCasObtenirInfosUnHoraire (String idTrajet, ArrayList<Integer> horaires, int[] repAtt) {
        int[] rep;
        rep = obtenirInfosUnHoraire(idTrajet, horaires);
        boolean egalite;
        egalite = Arrays.equals(rep, repAtt);
        if (egalite) {   
            System.out.println("Test réussi");
        } else {
            System.out.println("Echec du test");            
        }
    }

    /**
    * Par une recherche dans la collection des trajets, cette méthode renvoie tous les trajets (identifiants des
    * trajets) dont la gare de départ est identique au paramètre gareDep (peu importe l’heure de départ car
    * ceci sera vérifié dans un second temps).
    * @param gareDep - la gare de départ que l'on rentre
    * @param trajets - la collection des trajets
    * @return la collection de tous les identifiants des trajets correspondants à la gare de départ
    */
    ArrayList<String> trouverTousLesTrajets (String gareDep, ArrayList<String> trajets){
        ArrayList<String> idTrajets = new ArrayList<String>();

        for (int i = 0; i < trajets.size(); i = i + 4) {
            if (gareDep.equals(trajets.get(i + 2))) {
                idTrajets.add(trajets.get(i));
            }
        }

        // Si il n'y a aucun élément dans la collection des idTrajets, on renvoie null.
        if (idTrajets.size() == 0) {
            idTrajets = null;
        }

        return idTrajets;
    }

    /**
    * test de la méthode trouverTousLesTrajets
    */
    void testTrouverTousLesTrajets () {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        remplirLesCollections (trajets, horaires, nomDuFichierInfos);
        
        System.out.println();
        System.out.println("testTrouverTousLesTrajets : ");

        //Cas Normaux
        //ATTENTION IL FAUT AJOUTER LES ID DES idTrajets DANS L'ORDRE QU'ILS SONT AJOUTES DANS LE FICHIER 

        ArrayList<String> idTrajets1 = new ArrayList<String>();
        idTrajets1.add("1");
        idTrajets1.add("2");
        idTrajets1.add("6");
        idTrajets1.add("7");
        idTrajets1.add("5");
        idTrajets1.add("8");
        testCasTrouverTousLesTrajets("Vannes", trajets, idTrajets1);

        ArrayList<String> idTrajets2 = new ArrayList<String>();
        idTrajets2.add("3");
        idTrajets2.add("4");
        testCasTrouverTousLesTrajets("Redon", trajets, idTrajets2);

        ArrayList<String> idTrajets3 = new ArrayList<String>();
        idTrajets3.add("10");
        idTrajets3.add("9");
        testCasTrouverTousLesTrajets("Rennes", trajets, idTrajets3);

        //Cas d'erreurs
        ArrayList<String> idTrajets4 = new ArrayList<String>();
        testCasTrouverTousLesTrajets("Nantes", trajets, idTrajets4);
    }

    /**
	* teste un appel de trouverTousLesTrajets : on vérifie que tous les id des trajets renvoyés ont bien la gare de départ
    * @param gareDep - la gare de départ que l'on rentre
    * @param trajets - la collection des trajets
	* @param repAtt - resultat attendu
	**/ 
    void testCasTrouverTousLesTrajets (String gareDep, ArrayList<String> trajets, ArrayList<String> repAtt) {
        ArrayList<String> idTrajets = new ArrayList<String>();
        idTrajets = trouverTousLesTrajets(gareDep, trajets);

        if(idTrajets == null) {
            System.out.println("La gare de départ n'existe pas : Echec du test");
        } else {
            boolean egalite = true;
            // On vérifie que les deux collections ont la même taille sinon si la collection repAtt contient au début tous 
            // les idTrajets de la collection idTrajets mais qu'après il y a d'autres éléments,
            // la méthode renverra true alors que ce n'est pas le cas.
            if (idTrajets.size() != repAtt.size()) {
                egalite = false;
            } else {
                for(int i = 0; egalite && i < idTrajets.size(); i++) {
                    if (idTrajets.get(i).equals(repAtt.get(i))) {
                        egalite = true;
                    } else {
                        egalite = false;
                    }
                }
            }
            if (egalite) {   
                System.out.println("Test réussi");
            } else {
                System.out.println("Echec du test");            
            }
        }
    }

    /**
    * Ecrire et tester une nouvelle méthode qui renvoie toutes les correspondances possibles dans une gare à partir d’une heure donnée.
    * @param gare - la gare de départ que l'on rentre
    * @param heure - l'heure de départ que l'on rentre
    * @param trajets - la collection des trajets
    * @return la collection de tous les identifiants des trajets correspondants à la gare de départ
    */
    ArrayList<String> chercherCorrespondances (String gare, Duree heure, ArrayList<String> trajets, ArrayList<Integer> horaires) {

        ArrayList<String> idTrajetsCorrespondances = new ArrayList<String>();
        //On récupère tous les trajets qui partent de la même gare
        ArrayList<String> idTrajets = new ArrayList<String>();
        idTrajets = trouverTousLesTrajets(gare, trajets);



        if(idTrajets != null) {
            //Pour chaque identifiant présent dans idTrajets, on le met dans idTrajetsCorrespondances si l'heure de départ est supérieure à l'heure de départ et la gare d'arrivée est différente de la gare de départ
            for(int i = 0; i < idTrajets.size(); i++) {
                //On récupère les infos de chaque trajet dans idTrajets avec la méthode obtenirInfosUntrajet
                String[] infosTrajet = new String[3];
                infosTrajet = obtenirInfosUnTrajet(idTrajets.get(i), trajets);

                //On récupère les heure de chaque trajet dans idTrajets avec la méthode obtenirInfosUnHoraire
                int[] infosHoraire = new int[4];
                infosHoraire = obtenirInfosUnHoraire(idTrajets.get(i), horaires);

                //On met les heures et les minutes de départ et d'arrivée du trajet correspondance dans une Duree
                Duree heureDepartCorrespondance = new Duree(infosHoraire[0], infosHoraire[1], 0);

                //Si l'heure de départ du trajet est supérieure à l'heure de départ que l'on rentre
                if(heureDepartCorrespondance.compareA(heure) == 1){
                    //On ajoute l'id du trajet à la collection des idTrajetsCorrespondances
                    idTrajetsCorrespondances.add(idTrajets.get(i));
                }
            }
        } else {
            idTrajetsCorrespondances = null;
        }
        
        return idTrajetsCorrespondances;
    }

    /**
    * test de la méthode chercherCorrespondances
    */
    void testChercherCorrespondance () {
        ArrayList<String> trajets = new ArrayList<String>();
        ArrayList<Integer> horaires = new ArrayList<Integer>();
        remplirLesCollections (trajets, horaires, nomDuFichierInfos);
        
        System.out.println();
        System.out.println("testChercherCorrespondance : ");

        //Cas Normaux
        Duree duree1 = new Duree(7,0,0); //Départ à 7h00 de Vannes
        ArrayList<String> idTrajets1 = new ArrayList<String>();
        idTrajets1.add("1");
        idTrajets1.add("2");
        idTrajets1.add("6");
        idTrajets1.add("7");
        idTrajets1.add("5");
        idTrajets1.add("8");
        testCasChercherCorrespondance("Vannes", duree1, trajets, horaires, idTrajets1);

        Duree duree2 = new Duree(9,0,0); //Départ à 9h00 de Vannes
        ArrayList<String> idTrajets2 = new ArrayList<String>();
        idTrajets2.add("1");
        //idTrajets2.add("2"); On ne prend pas le trajet 2 car l'heure de départ est inférieure à l'heure de départ que l'on rentre
        idTrajets2.add("6");
        idTrajets2.add("7");
        idTrajets2.add("5");
        testCasChercherCorrespondance("Vannes", duree2, trajets, horaires, idTrajets2);

        Duree duree3 = new Duree(18,0,0); //Départ à 18h00 de Vannes = aucun train ne part de Vannes après cette heure
        ArrayList<String> idTrajets3 = new ArrayList<String>();
        testCasChercherCorrespondance("Vannes", duree3, trajets, horaires, idTrajets3);

        Duree duree4 = new Duree(12,35,0); //Départ à 12h35 de Redon
        ArrayList<String> idTrajets4 = new ArrayList<String>();
        //idTrajets4.add("3"); On ne prend pas le trajet 3 car l'heure de départ est inférieure à l'heure de départ que l'on rentre
        idTrajets4.add("4");
        testCasChercherCorrespondance("Redon", duree4, trajets, horaires, idTrajets4);

        //Cas d'erreurs
        Duree duree5 = new Duree(1,0,0); //Départ à 1h00 de Nantes = aucun train ne part de Nantes 
        ArrayList<String> idTrajets5 = new ArrayList<String>();
        testCasChercherCorrespondance("Nantes", duree5, trajets, horaires, idTrajets5);
    }

    /**
	* teste un appel de chercherCorrespondance : on vérifie que tous les id des trajets renvoyés correspondent bien à ceux attendus
    * @param gare - la gare de départ que l'on rentre
    * @param heure - l'heure de départ que l'on rentre
    * @param trajets - la collection des trajets
    * @param horaires - la collection des horaires
	* @param repAtt - resultat attendu
	**/ 
    void testCasChercherCorrespondance (String gare, Duree heure, ArrayList<String> trajets, ArrayList<Integer> horaires, ArrayList<String> repAtt) {
        ArrayList<String> idTrajets = new ArrayList<String>();
        idTrajets = chercherCorrespondances(gare, heure, trajets, horaires);
        
        if(idTrajets == null) {
            System.out.println("La gare de départ n'existe pas : Echec du test");
        } else {
            int i = 0;
            boolean egalite = true;
            while (egalite && i < idTrajets.size()) {
                if(idTrajets.get(i).equals(repAtt.get(i))) {
                    egalite = true;
                } else {
                    egalite = false;
                }
                i++;
            }
            if (egalite) {   
                System.out.println("Test réussi");
            } else {
                System.out.println("Echec du test");            
            }
        }
    }
}